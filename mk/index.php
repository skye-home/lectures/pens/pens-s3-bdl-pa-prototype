<?php 
	include '../connection.php';

	$sql = ociparse($conn, "SELECT * FROM mata_kuliah");
	oci_execute($sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Daftar Mata Kuliah</title>
</head>
<body>
	<h3>Daftar Mata Kuliah</h3>
	<div>
		<a href="./create.php">Tambah</a>
	</div>
	<table border="1">
		<thead>
			<tr>
				<th>No.</th>
				<th>Nama</th>
				<th>Deskripsi</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 0; ?>
			<?php while ($row = oci_fetch_array($sql)) :?>
			<tr>
				<td><?= ++$i ?></td>
				<td><?= $row["MATA_KULIAH_NAMA"] ?></td>
				<td><?= $row["MATA_KULIAH_DESKRIPSI"] ?></td>
				<td>
					<a href="./update.php?mata_kuliah=<?= $row['MATA_KULIAH_ID'] ?>">Ubah</a>
					<a href="./delete.php?mata_kuliah=<?= $row['MATA_KULIAH_ID'] ?>">Hapus</a>
				</td>
			</tr>
			<?php endwhile ?>
		</tbody>
	</table>
</body>
</html>