<?php 
	include "../connection.php";

	if (isset($_POST["submit"])) {
		$mId = $_POST["mata_kuliah_id"];
		$mNama = $_POST["mata_kuliah_nama"];
		$mDes = $_POST["mata_kuliah_deskripsi"];

		$sql = ociparse($conn, "declare begin p_mata_kuliah($mId, '$mNama', '$mDes', 'insert'); end;");
		ociexecute($sql);

		if (oci_num_rows($sql) > 0) {
			echo "
				<script>alert('mata kuliah berhasil ditambahkan');
					document.location.href = 'index.php';
				</script>
			";
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Tambah Mata Kuliah</title>
</head>
<body>
	<h3>Tambah Mata Kuliah</h3>
	<a href="./index.php">Kembali</a>
	<form method="POST">
		<div>
			<label for="mata_kuliah_id">ID</label>
			<input type="number" name="mata_kuliah_id" id="mata_kuliah_id">
		</div>
		<div>
			<label for="mata_kuliah_nama">Nama</label>
			<input type="text" name="mata_kuliah_nama" id="mata_kuliah_nama">
		</div>
		<div>
			<label for="mata_kuliah_deskripsi">Deskripsi</label>
			<input type="text" name="mata_kuliah_deskripsi" id="mata_kuliah_deskripsi">
		</div>
		<button type="submit" name="submit">Simpan</button>
	</form>
</body>
</html>