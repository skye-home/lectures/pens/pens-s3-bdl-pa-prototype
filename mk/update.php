<?php 
	include "../connection.php";

	$id = $_GET["mata_kuliah"];

	$sql = ociparse($conn, "SELECT * FROM mata_kuliah WHERE mata_kuliah_id=" . $id);
	oci_execute($sql);
	$row = oci_fetch_array($sql);

	if (isset($_POST["submit"])) {
		$mId = $_POST["mata_kuliah_id"];
		$mNama = $_POST["mata_kuliah_nama"];
		$mDes = $_POST["mata_kuliah_deskripsi"];

		$sql = ociparse($conn, "declare begin p_mata_kuliah($mId, '$mNama', '$mDes', 'update'); end;");
		ociexecute($sql);

		if (oci_num_rows($sql) > 0) {
			echo "
				<script>alert('mata kuliah berhasil diubah');
					document.location.href = 'index.php';
				</script>
			";
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Ubah Mata Kuliah</title>
</head>
<body>
	<h3>Ubah Mata Kuliah</h3>
	<a href="./index.php">Kembali</a>
	<form method="POST">
		<input type="hidden" name="mata_kuliah_id" value="<?= $id ?>">
		<div>
			<label for="mata_kuliah_nama">Nama</label>
			<input type="text" name="mata_kuliah_nama" id="mata_kuliah_nama" value="<?= $row['MATA_KULIAH_NAMA'] ?>">
		</div>
		<div>
			<label for="mata_kuliah_deskripsi">Deskripsi</label>
			<input type="text" name="mata_kuliah_deskripsi" id="mata_kuliah_deskripsi" value="<?= $row['MATA_KULIAH_DESKRIPSI'] ?>">
		</div>
		<button type="submit" name="submit">Ubah</button>
	</form>
</body>
</html>