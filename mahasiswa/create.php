<?php 
	include "../connection.php";

	if (isset($_POST["submit"])) {
		$mId = $_POST["mahasiswa_id"];
		$mNrp = $_POST["mahasiswa_nrp"];
		$mNama = $_POST["mahasiswa_nama"];

		$sql = ociparse($conn, "declare begin p_mahasiswa($mId, '$mNrp', '$mNama', 'insert'); end;");
		ociexecute($sql);

		if (oci_num_rows($sql) > 0) {
			echo "
				<script>alert('mahasiswa berhasil ditambahkan');
					document.location.href = 'index.php';
				</script>
			";
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Tambah Mahasiswa</title>
</head>
<body>
	<h3>Tambah Mahasiswa</h3>
	<a href="./index.php">Kembali</a>
	<form method="POST">
		<div>
			<label for="mahasiswa_id">ID</label>
			<input type="number" name="mahasiswa_id" id="mahasiswa_id">
		</div>
		<div>
			<label for="mahasiswa_nrp">NRP</label>
			<input type="text" name="mahasiswa_nrp" id="mahasiswa_nrp">
		</div>
		<div>
			<label for="mahasiswa_nama">Nama</label>
			<input type="text" name="mahasiswa_nama" id="mahasiswa_nama">
		</div>
		<button type="submit" name="submit">Simpan</button>
	</form>
</body>
</html>