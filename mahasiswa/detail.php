<?php 
	include "../connection.php";

	$id = $_GET["mahasiswa"];

	$sql = ociparse($conn, "SELECT * FROM mahasiswa WHERE mahasiswa_id=" . $id);
	oci_execute($sql);
	$row = oci_fetch_array($sql);

	$sql2 = ociparse($conn, "
		SELECT mmk.mahasiswa_mata_kuliah_id, mk.mata_kuliah_id, mk.mata_kuliah_nama, mk.mata_kuliah_deskripsi, mn.mahasiswa_nilai_nilai
            FROM mata_kuliah mk JOIN mahasiswa_mata_kuliah mmk
                ON mk.mata_kuliah_id = mmk.mahasiswa_mata_kuliah_mata_kuliah_id 
            LEFT JOIN mahasiswa_nilai mn
                ON mk.mata_kuliah_id = mn.mahasiswa_nilai_mata_kuliah_id 
                	AND mmk.mahasiswa_mata_kuliah_mahasiswa_id = mn.mahasiswa_nilai_mahasiswa_id
		WHERE mmk.mahasiswa_mata_kuliah_mahasiswa_id=" . $id);
	ociexecute($sql2);

	$sql3 = ociparse($conn, "SELECT * FROM mata_kuliah");
	ociexecute($sql3);

	if (isset($_POST["add-mk-submit"])) {
		$mkId = $_POST["add-mk-id"];

		$randId = rand(100, 10000);

		$add_mk_sql = ociparse($conn, "declare begin p_mahasiswa_mata_kuliah($randId, $id, $mkId, 'insert'); end;");
		ociexecute($add_mk_sql);

		if (oci_num_rows($add_mk_sql) > 0) {
			echo "
				<script>alert('mata kuliah berhasil ditambahkan');
					document.location.href = 'detail.php?mahasiswa=$id';
				</script>
			";
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Detail Mahasiswa</title>
</head>
<body>
	<h3>Detail Mahasiswa</h3>
	<div>
		<span>NRP:</span>
		<span><?= $row["MAHASISWA_NRP"] ?></span>
		<br>
		<span>Nama:</span>
		<span><?= $row["MAHASISWA_NAMA"] ?></span>
		<br>

		<form method="POST">
			<select name="add-mk-id">
				<?php while ($row = oci_fetch_array($sql3)) :?>
					<option value="<?= $row['MATA_KULIAH_ID'] ?>"><?= $row["MATA_KULIAH_NAMA"] ?></option>
				<?php endwhile ?>
			</select>
			<button type="submit" name="add-mk-submit">Tambah MK</button>
		</form>
		
		<table border="1">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Nilai</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 0; ?>
				<?php while ($row = oci_fetch_array($sql2)) :?>
				<tr>
					<td><?= ++$i ?></td>
					<td><?= $row["MATA_KULIAH_NAMA"] ?></td>
					<td><?= $row["MAHASISWA_NILAI_NILAI"] ?></td>
					<td>
						<a href="./in-score.php?mahasiswa=<?= $id ?>&mata_kuliah=<?= $row['MATA_KULIAH_ID'] ?>">Score</a>
						<a href="./mk-delete.php?mahasiswa=<?= $id ?>&mata_kuliah=<?= $row['MATA_KULIAH_ID'] ?>">Delete</a>
					</td>
				</tr>
				<?php endwhile ?>
			</tbody>
		</table>
	</div>
</body>
</html>