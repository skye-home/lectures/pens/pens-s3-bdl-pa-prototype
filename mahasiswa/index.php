<?php 
	include '../connection.php';

	$sql = ociparse($conn, "SELECT * FROM mahasiswa");
	oci_execute($sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Daftar Mahasiswa</title>
</head>
<body>
	<h3>Daftar Mahasiswa</h3>
	<div>
		<a href="./create.php">Tambah</a>
	</div>
	<table border="1">
		<thead>
			<tr>
				<th>No.</th>
				<th>NRP</th>
				<th>Nama</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 0; ?>
			<?php while ($row = oci_fetch_array($sql)) :?>
			<tr>
				<td><?= ++$i ?></td>
				<td><?= $row["MAHASISWA_NRP"] ?></td>
				<td><a href="detail.php?mahasiswa=<?= $row['MAHASISWA_ID'] ?>"><?= $row["MAHASISWA_NAMA"] ?></a></td>
				<td>
					<a href="./update.php?mahasiswa=<?= $row['MAHASISWA_ID'] ?>">Ubah</a>
					<a href="./delete.php?mahasiswa=<?= $row['MAHASISWA_ID'] ?>">Hapus</a>
				</td>
			</tr>
			<?php endwhile ?>
		</tbody>
	</table>
</body>
</html>