<?php 
	include "../connection.php";

	$id = $_GET["mahasiswa"];

	$sql = ociparse($conn, "SELECT * FROM mahasiswa WHERE mahasiswa_id=" . $id);
	oci_execute($sql);
	$row = oci_fetch_array($sql);

	if (isset($_POST["submit"])) {
		$mId = $_POST["mahasiswa_id"];
		$mNrp = $_POST["mahasiswa_nrp"];
		$mNama = $_POST["mahasiswa_nama"];

		$sql = ociparse($conn, "declare begin p_mahasiswa($mId, '$mNrp', '$mNama', 'update'); end;");
		ociexecute($sql);

		if (oci_num_rows($sql) > 0) {
			echo "
				<script>alert('mahasiswa berhasil diubah');
					document.location.href = 'index.php';
				</script>
			";
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Ubah Mahasiswa</title>
</head>
<body>
	<h3>Ubah Mahasiswa</h3>
	<a href="./index.php">Kembali</a>
	<form method="POST">
		<input type="hidden" name="mahasiswa_id" value="<?= $row['MAHASISWA_ID'] ?>">
		<div>
			<label for="mahasiswa_nrp">NRP</label>
			<input type="text" name="mahasiswa_nrp" id="mahasiswa_nrp" value="<?= $row['MAHASISWA_NRP'] ?>">
		</div>
		<div>
			<label for="mahasiswa_nama">Nama</label>
			<input type="text" name="mahasiswa_nama" id="mahasiswa_nama" value="<?= $row['MAHASISWA_NAMA'] ?>">
		</div>
		<button type="submit" name="submit">Ubah</button>
	</form>
</body>
</html>